<?php

/**
 * @file
 * Hooks provided by the Entity Confirmation module.
 */

use Drupal\Core\Entity\EntityInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter entity confirmation message.
 *
 * @param string $value
 *   The confirmation message value.
 * @param string $op
 *   The entity operation.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity.
 */
function hook_entity_confirmation_alter(string &$value, string $op, EntityInterface $entity): void {
  $value = t('New confirmation message!');
}

/**
 * @} End of "addtogroup hooks".
 */
