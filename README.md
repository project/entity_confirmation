# Entity Confirmation

Often times the default entity save message is insufficient or too boring. Customize the default message shown after
the entity create, edit, and delete operations are completed. Configurations for each operation are available on
individual form modes for maximum flexibility. See `entity_confirmation.api.php` for documentation on the available
entity confirmation alters.

## Usage

1. Download and install the `drupal/entity_confirmation` module. Recommended install method is composer:
   ```
   composer require drupal/entity_confirmation
   ```
2. Go to the "Manage form display" tab of the desired entity type.
3. Choose a form mode to customize.
4. Review available configurations under the confirmation settings section and save changes.
